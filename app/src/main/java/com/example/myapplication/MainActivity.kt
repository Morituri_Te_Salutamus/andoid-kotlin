package com.example.myapplication


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

private lateinit var textView: TextView
private lateinit var editText: EditText

public class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.textView)
        editText = findViewById(R.id.inputForEditing)
        editText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }


    fun checkEditTextOnNull(): String{
        val textUrl = getString(R.string.urlLurk)
        if (editText.text == null || editText.text.length ==0){
            editText.setText("m.lurkmore.to")
        }
        return textUrl.format(editText.text)

    }
    //Медот открытия юрл
    fun openUrl(view: View) {
        val urlAct = Intent(this,UrlActivit::class.java)
        urlAct.putExtra(UrlActivit.TEXT_FROM_EDIT, checkEditTextOnNull())
        startActivity(urlAct)
    }




    //Показываем тоаст
//    fun toastMe(view: View){
//        val meToast = Toast.makeText(this, getString(R.string.toastText, editText.text), Toast.LENGTH_LONG)
//        meToast.show()
//    }


    //Метод увелечения числа на 1
    fun countMe(view: View) {
        val countString = textView.text.toString()
        var count = Integer.parseInt(countString)
        count++
        textView.text = count.toString()
    }


    //метод рандома
    fun randomMe(view: View) {
        val randomIntent = Intent(this, secondActivity::class.java)

        val countString = textView.text.toString()

        val count = Integer.parseInt(countString)

        randomIntent.putExtra(secondActivity.TOTAL_COUNT, count)

        startActivity(randomIntent)

    }

}