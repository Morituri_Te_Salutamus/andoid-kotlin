package com.example.myapplication

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity



class UrlActivit : AppCompatActivity() {
    companion object{
        const val TEXT_FROM_EDIT : String = "text_from_edit"
    }

    private val webView by lazy { findViewById<WebView>(R.id.webView) }
    private val progressBar by lazy { findViewById<View>(R.id.progressBar) }
    private val rndButton by lazy {findViewById<View>(R.id.RNDLRK)}



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_url)

        loadPage()
        configureWebView()

        rndButton.setOnClickListener { loadPage() }

        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
               // progressBar.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                println("onPageStarted")
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                view?.loadUrl(url)
                println("shouldOverrideUrlLoading")
                progressBar.visibility = View.VISIBLE
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                println("onPageFinished")

            }

        }
    }

    private fun loadPage() {
        val textFromEdit = intent.getStringExtra(TEXT_FROM_EDIT)
        webView.loadUrl(textFromEdit.toString())
    }

    private fun configureWebView() {
        val webSettings = webView.settings;
        webSettings.javaScriptEnabled = true;
        webSettings.builtInZoomControls = true;
        webSettings.loadWithOverviewMode = true;
        webSettings.useWideViewPort = true;
    }

}